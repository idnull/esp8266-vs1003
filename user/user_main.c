/*
 *  Example of working sensor DHT22 (temperature and humidity) and send data on the service thingspeak.com
 *  https://thingspeak.com
 *
 *  For a single device, connect as follows:
 *  DHT22 1 (Vcc) to Vcc (3.3 Volts)
 *  DHT22 2 (DATA_OUT) to ESP Pin GPIO2
 *  DHT22 3 (NC)
 *  DHT22 4 (GND) to GND
 *
 *  Between Vcc and DATA_OUT need to connect a pull-up resistor of 10 kOh.
 *
 *  (c) 2015 by Mikhail Grigorev <sleuthhound@gmail.com>
 *
 */

#include <user_interface.h>
#include <osapi.h>
#include <c_types.h>
#include <mem.h>
#include <os_type.h>
#include "httpclient.h"
#include "driver/uart.h"
#include "user_config.h"

#include "gpio.h"
#include "eagle_soc.h"
#include "wifi_api.c"

#include "driver/vs1003.h"



static ETSTimer WiFiLinker;


unsigned char *default_certificate;
unsigned int default_certificate_len = 0;
unsigned char *default_private_key;
unsigned int default_private_key_len = 0;

//char radio[]={"http://shoutcast.byfly.by:88/radiominsk"};
char radio[]={"http://shoutcast.byfly.by:88/nashe128"};
//char radio[]={"http://nashe1.hostingradio.ru:80/best-128.mp3"};
//char radio[]={"http://shoutcast.byfly.by:88/radiorecord320"};
//char radio[]={"http://shoutcast.byfly.by:88/181rock40"};
//char radio[]={"http://air.radiorecord.ru:8101/rr_320"};
//char radio[]={"http://shoutcast.byfly.by:88/euronews"};







LOCAL void ICACHE_FLASH_ATTR radio_http_callback(char * response, int http_status, char * full_response)
{
	// nothing
}


void ICACHE_FLASH_ATTR mp3_callback(char * buf, unsigned short len)
{

	int i;

	if        (((buf[0] == 'I') && (buf[1] == 'C') && (buf[2] == 'Y'))
			|| ((buf[0] == 'H') && (buf[1] == 'T') && (buf[2] == 'T') && (buf[3] == 'P')))
	{

		// print ICY header
		os_printf("%s", buf);
		return;
	}

	system_update_cpu_freq(SYS_CPU_160MHZ);

		Mp3DeselectControl();
		Mp3SelectData();
		for (i=0;i<len;)
		{
			if(MP3_DREQ)
			SPIPutChar(buf[i++]);
		}
		Mp3DeselectData();

	system_update_cpu_freq(SYS_CPU_80MHZ);

}

static void ICACHE_FLASH_ATTR wifi_check_ip(void *arg)
{

	static struct ip_info ipConfig;
	os_timer_disarm(&WiFiLinker);
	switch(wifi_station_get_connect_status())
	{
		case STATION_GOT_IP:
			wifi_get_ip_info(STATION_IF, &ipConfig);
						if(ipConfig.ip.addr != 0) {

							http_get(radio,"", radio_http_callback);
							//:TODO
							// MAKE RECONNECT WHEN CONNECTION LOST
							return;
						} else {
							os_printf("WiFi connected, ip.addr is null\r\n");
						}
			break;
		case STATION_WRONG_PASSWORD:

			os_printf("WiFi connecting error, wrong password\r\n");
			break;
		case STATION_NO_AP_FOUND:

			os_printf("WiFi connecting error, ap not found\r\n");
			break;
		case STATION_CONNECT_FAIL:

			os_printf("WiFi connecting fail\r\n");
			break;
		default:

			os_printf("WiFi connecting...\r\n");
	}
	os_timer_setfn(&WiFiLinker, (os_timer_func_t *)wifi_check_ip, NULL);
	os_timer_arm(&WiFiLinker, 500, 0);
}


void user_rf_pre_init(void)
{

}

void user_init(void)
{
	// Configure the UART
	uart_init(BIT_RATE_115200,0);
	// Enable system messages
	system_set_os_print(1);

	if(wifi_get_opmode() != STATION_MODE)
	{
		setup_wifi_st_mode();
	}
	os_printf("Wifi setup ok\n\r");

	VS1003_Config();
	Mp3Reset();
	os_delay_us(100);
	Mp3SetVolume(1,1);
	Mp3DeselectControl();

// lcd init and test
	i2c_init();
	i2c_start();
	i2c_stop();

   	LCD_Init();
   	os_delay_us(1000);
   	LCD_Clear();
   	os_delay_us(1000);


	LCD_GotoXY(0,1);
	LCD_FStr(radio);
	LCD_Update();


//		LCD_2xFStr("Hello! :)"+0);
//		LCD_GotoXY(0,4);
//		LCD_FStr("����������, ���!"+0);
//		LCD_GotoXY(0,5);
//		LCD_FStr("0123456789"+0);
//		LCD_GotoXY(0,3);
//		LCD_2xFStr("0123456789"+0);
//		LCD_Update();


	os_timer_disarm(&WiFiLinker);
	os_timer_setfn(&WiFiLinker, (os_timer_func_t *)wifi_check_ip, NULL);
	os_timer_arm(&WiFiLinker, 100, 0);

}



